package edu.towson.cosc435.homayouni.assignment5cosc435.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface photoDao {

    @Query("SELECT * from photo WHERE id = :id")
    fun getPhoto(id: Int): List<Photo>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(photo: Photo)

    @Update
    suspend fun update(photo: Photo)

    @Delete
    suspend fun delete(photo: Photo)
}