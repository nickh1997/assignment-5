package edu.towson.cosc435.homayouni.assignment5cosc435

import androidx.room.Database
import androidx.room.RoomDatabase
import edu.towson.cosc435.homayouni.assignment5cosc435.data.Photo
import edu.towson.cosc435.homayouni.assignment5cosc435.data.photoDao

@Database(entities = [Photo::class], version = 1, exportSchema = false)
abstract class PhotoRoomDatabase : RoomDatabase() {

    abstract fun photoDao(): photoDao

    companion object {

    }
}