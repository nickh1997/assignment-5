package edu.towson.cosc435.homayouni.assignment5cosc435

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import java.lang.Exception

class PhotoViewModel : ViewModel() {
    private val _photoList = mutableStateListOf<Photo>()
    var errorMessage: String by mutableStateOf("")
    val photoList: List<Photo>
    get() = _photoList

    // function for view to fetch photos
    fun getPhotoList() {
        viewModelScope.launch {
            val apiService = APIService.getInstance()
            try {
                _photoList.clear()
                _photoList.addAll(apiService.getPhotos())
            } catch (ex: Exception) {
                errorMessage = ex.message.toString()
            }
        }
    }
}