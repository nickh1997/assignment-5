package edu.towson.cosc435.homayouni.assignment5cosc435

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

data class Photo(
    var albumId: Int,
    var id: Int,
    var title: String,
    var url: String,
    var thumbnailUrl: String
)
// specify base url to fetch
const val URL = "https://jsonplaceholder.typicode.com/photos/"

interface APIService {
    // GET request to fetch all photos
    // deserialize into list of photo
    @GET("photos")
    suspend fun getPhotos(): List<Photo>

    companion object {
        var apiService: APIService? = null
        fun getInstance(): APIService {
            if (apiService == null) {
                apiService = Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory((GsonConverterFactory.create()))
                    .build().create(APIService::class.java)
            }
            return apiService!!
        }
    }
}