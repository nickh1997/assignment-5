package edu.towson.cosc435.homayouni.assignment5cosc435

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import edu.towson.cosc435.homayouni.assignment5cosc435.ui.theme.Assignment5COSC435Theme

class MainActivity : ComponentActivity() {
    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Assignment5COSC435Theme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    val vm = PhotoViewModel()
                    PhotoView(vm)
                }
            }
        }
    }
}

@ExperimentalFoundationApi
@Composable
// function for the ViewModel
// list displayed in view
fun PhotoView(vm: PhotoViewModel) {

    val nav = rememberNavController()
    val scaffoldState= rememberScaffoldState()
    val scope = rememberCoroutineScope()

    // view makes api call
    LaunchedEffect(Unit, block = {
        vm.getPhotoList()
    })

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Row {
                        Text("Assignment 5")
                    }
                }
            )
        },
        scaffoldState = scaffoldState,
        content = {
            if (vm.errorMessage.isEmpty()) {
                Column(modifier = Modifier.padding(16.dp)) {
                    LazyColumn(modifier = Modifier.fillMaxHeight()) {
                        items(vm.photoList) { photo ->
                            Column {
                                Row(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(16.dp),
                                    horizontalArrangement = Arrangement.SpaceBetween
                                ) {
                                    Box(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding()
                                    ) {
                                        Text(
                                            photo.title,
                                            maxLines = 1,
                                            overflow = TextOverflow.Ellipsis
                                        )
                                    }
                                    Spacer(modifier = Modifier.width(16.dp))
                                }
                                Divider()
                            }
                        }
                    }
                }
            } else {
                Text(vm.errorMessage)
            }
        },
    )
}

@Composable
fun Main(
    nav: NavHostController,
    showSnack: (String) -> Unit
) {
    val navController = rememberNavController()

    NavHost(
        navController = nav,
        startDestination = NewPhotoScreen.Home.route
    ) {
        composable(NewPhotoScreen.Home.route) {
            Home()
        }
        composable(NewPhotoScreen.NewPhoto.route) {
            NewPhoto(showSnack)
        }
    }
}

@Composable
fun Home() {                  // Home is first screen
    Column(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally) {
        }
    }
}

@Composable
fun NewPhoto(navController: (String) -> Unit) {
    Column(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally) {
        }
    }
}

private val screen = listOf(
    NewPhotoScreen.Home,
    NewPhotoScreen.NewPhoto
)

sealed class NewPhotoScreen(val title: String, val route: String) {
    object Home : NewPhotoScreen("Home", "home")
    object NewPhoto : NewPhotoScreen("NewPhoto", "newPhoto")
}

